# Laporan Proyek Machine Learning - Yunus Bayu Aji

## Domain Proyek
<div style="text-align: justify;">

Industri teknologi mempunyai perkembangan yang cepat dan masif, tidak terkecuali industri *PC* atau Laptop. Pengetahuan mengenai harga laptop dapat memberikan strategi yang efektif dari sisi konsumen untuk menentukan harga yang optimal yang sesuai dengan preferensi dan kebutuhan.

Dengan mempunyai kemampuan untuk memprediksi harga laptop yang akurat, konsumen dapat mengambil keputusan dengan lebih cerdas, merencanakan anggaran dengan lebih efisien, mengetahui nilai tawar yang sesuai, serta menghindari segala bentuk penipuan oleh oknum penjual.

Prediksi harga laptop dapat dilakukan dengan melakukan pendekatan dari pola - pola data seperti spesifikasi teknis dan fitur - fitur yang ditawarkan. Teknik analisis memungkinkan untuk memprediksi harga dengan mempelajari faktor - faktor yang mempengaruhi. Pemahaman yang baik mengenai pengolahan data akan memberikan hasil yang lebih baik ketika melakukan pengambilan keputusan dapat lebih akurat.
</div>

## Business Understanding
### Problem Statements
<div style="text-align: justify;">

- Bagaimana konsumen dapat lebih mudah menentukan harga tawar yang sesuai?

- Bagaimana cara meningkatkan keandalan informasi tentang harga laptop dengan spesifikasi tertentu yang sesuai dengan preferensi konsumen?

### Goals

- Memudahkan konsumen dalam menentukan harga tawar yang sesuai dengan kebutuhan dan preferensi mereka.

- Meningkatkan keandalan informasi tentang harga laptop dengan spesifikasi tertentu yang sesuai dengan preferensi konsumen.

### Solution statements
  - Menggunakan beberapa algoritma untuk melakukan pendekatan pola harga laptop seperti *Random Forest*, *Gradient Boosting*, *Adaptive Boosting*, *SVM*, dan *KNN*. Prediksi harga laptop yang akurat didapatkan dengan memilih model terbaik.
  - Kinerja dan akurasi model diukur dengan metriks evaluasi *Root Mean Squared Error (RMSE)*
</div>

## Data Understanding

<div style="text-align: justify;">

Data yang digunakan adalah data harga laptop yang memuat spesifikasi 16 data teknis laptop dan 2 data jumlah *rating* dan *review*. 

Dataset : [Laptop Price Dataset](https://www.kaggle.com/code/anubhavgoyal10/laptop-price-prediction/input)

### Variabel-variabel pada Laptop Price dataset adalah sebagai berikut:

- brand : Perusahaan tempat pembuatan laptop
- processor_brand : Merek *processor*
- processor_name  : Nama *processor*
- processor_gnrtn : Generasi *processor*
- ram_gb  : Ukuran *RAM*
- ram_type  : Tipe *RAM*
- ssd : ukuran *SSD*
- hdd : ukuran *HDD*
- os  : Tipe *Operating System*
- os_bit  : Versi *bit OS*
- graphic_card_gb : Ukuran *Graphic Card*
- Weight : Berat Laptop
- Warranty : Masa garansi laptop
- TouchScreen : Kemampuan laptop untuk bisa *touch screen* atau tidak
- msoffice : *MS office* tersedia atau tidak
- Price : Harga (dalam rupee india)
- rating : Nilai *rating* (1-5 *starts*)
- Number of Ratings : Jumlah *rating* yang diberikan
- Number of Reviews : Jumlah *review* yang diberikan

Untuk memahami data lebih lanjut maka digunakan teknik *Exploratory Data Analysis(EDA)*. *EDA* meliputi beberapa fungsi seperti : Deskripsi Variabel, Menangani *Missing Value*, *Univariate Analysis*, *Multivariate Analysis*, penjelasannya adalah sebagai berikut:

- Deskripsi Variabel :
  Tahap ini dilakukan untuk melihat secara lebih detail tentang data, mulai dari informasi data, tipe data, deskripsi data, dan ukuran data

- *Univariate Analysis* :
  Tahap ini dilakukan dengan mendefinisikan variabel numerikal dan kategorikal. Untuk data kategorikal dilakukan visualisasi data berupa bar, dan persentase jumlah setiap komponen data. Untuk data numerikal dilakukan visualisasi berupa histogram.

- *Multivariate Analysis* :
  Tahap ini dilakukan dengan melakukan operasi berupa korelasi antara tiap variabel, dalam hal ini, jika korelasi >-0.5 dan korelasi <0.5, maka variabel akan dihilangkan
</div>

## Data Preparation
<div style="text-align: justify;">

Pada bagian ini, data akan dilakukan proses eliminasi untuk menghilangkan nilai yang salah. Preparasi data dilakukan dengan melakukan *Encoding* data kategorik dan membagi data menjadi data train dan test.

- Tahap pertama preparasi data adalah menangani *missing value*:
Tahap ini dilakukan dengan beberapa operasi seperti menghilangkan duplikasi data dan menghilangkan *outlier* data. proses menghilangkan *outlier* data dilakukan menggunakan metode *IQR*. Metode ini akan melakukan identifikasi terhadap nilai yang berada di luar Q1(kuartil satu) dan Q3(kuartil tiga) dengan toleransi sebesar 1.5 dari nilai *IQR*, nilai *IQR* didapatkan dengan mengurangkan nilai Q3 dan Q1.

- Tahap kedua preparasi data adalah dengan melakukan *encoding data* pada data kategorikal. *encoding* dilakukan dengan menggunakan metode *One-Hot Encoding* dengan mengubah setiap data kategorikal menjadi representasi biner.

- Tahap ketiga yaitu membagi data menjadi data *train* dan *test*. data hasil pembagian ini yang kemudian akan dilatih menggunakan algoritma pengenalan pola yang sudah disebutkan di *solution statements*. Perbandingan data *train* dan *test* adalah 4:1.

- Alasan mengapa diperlukan tahapan *data preparation* tersebut adalah :
  - *Machine learning* pada dasarnya adalah algoritma yang melakukan operasi data numerik, sehingga data - data kategorikal pada *Laptop Price dataset* yang direpresentasikan oleh *variabel string* akan diubah menjadi *variabel numerik*, dala hal ini menggunakan *One-Hot Encoding*.
  - Pembagian data *train* dan *test* dilakukan untuk membangun sebuah model data yang mempunyai performa yang baik. data *train* digunakan untuk melatih model dan data *test* digunakan untuk *testing model*, dengan menggunakan data terpisah ini, dapat dilihat sejauh mana model dapat memprediksi data yang belum diketahui sebelumnya. 
  </div>

## Modeling
<div style="text-align: justify;">

*Modelling* merupakan tahapan untuk melakukan definisi dari model *machine learning* yang digunakan untuk mempelajari pola - pola data. Algoritma *machine learning* akan dijelaskan sebagai berikut :

1. *K-Nearest Neighbors (KNN)*:

  a. Algoritma ini menggunakan teknik regresi yaitu dengan melakukan prediksi berdasarkan nilai dari tetangga terdekat.

  b. Parameter : 

  - n_neighbors : Variabel jumlah tetangga terdekat yang digunakan untuk proses prediksi adalah 15.

  c. Kelebihan:

  - Algoritma cenderung sederhana 
  - Tidak diperlukan asumsi terkait distribusi data

  d. Kekurangan:

  - Algoritma ini cenderung sensitif terhadap data yang besar.
  - Algoritma akan berjalan lambat jika data sangat besar.

2. *Random Forest*:

  a. Merupakan algoritma yang terdiri dari beberapa *tree* atau pohon keputusan. Setiap *tree* akan ditanam pada suatu sampel acak dari data dan prediksi dihasilkan dari hasil keseluruhan prediksi pohon.

  b. Parameter:

  - n_estimators : Jumlah pohon keputusan yang digunakan adalah 100
  - max_depth : Maksimum kedalaman pohon adalah 7
  - n_jobs : jumlah job yang digunakan untuk melakukan pemrosesan secara pararel adalah -1, sehingga seluruh proses berjalan secara pararel.
  
  c. Kelebihan:

  - Mempunyai penanganan yang baik terhadap data yang kompleks
  - Cenderung lebih stabil dalam mengatasi *overfitting*
  
  d. Kekurangan:

  - Memerlukan waktu yang lama dalam melatih model.
  - Tidak memberikan interpretasi langsung tentang hubungan antara fitur dan target

3. AdaBoost:

  a. Merupakan algoritma ensemble yang menggabungkan antara beberapa model lemah menjadi model kuat

  b. Parameter:
  
  - n_estimators : Jumlah model lemah yang digunakan adalah 100.
  - learning_rate : Tingkat pembelajaran yang digunakan untuk pelatihan, ini digunakan untuk mengontrol pembelajaran pada model lemah adalah 0.001.

  c. Kelebihan:

  - Baik digunakan dalam data yang kompleks
  - Baik digunakan dalam kelas yang *imbalance*

  d. Kekurangan:

  - Jika kompleksitas suatu model terlalu tinggi, algoritma ini cenderung rentan terhadap *overfitting*
  - Sensitif terhadap *noise*

4. *Gradient Boosting*:

  a. Algoritma yang membangun model secara terurut dengan melakukan koreksi pada *error* model sebelumnya.

  b. Parameter:

  - n_estimators : Jumlah estimator adalah 100
  - learning_rate : Tingkat pembelajaran yang digunakan untuk pelatihan adalah 0.001.

  c. Kelebihan:
  
  - Efektif digunakan dalam data kompleks dan tidak linear
  - Mampu menyesuaikan model dengan mengatur *learning rate*.

  d. Kekurangan:

  - Mudah *overfitting* jika terlalu banyak *tree*
  - Memerlukan waktu yang cenderung lebih lama karena kompleksitas yang lebih tinggi.

5. *Support Vector Machine (SVM)*:

  a. *SVM* digunakan untuk melakukan regresi dengan mencari *hyperplane* terbaik yang memisahkan dua kelas.

  b. Parameter:
  
  - C : Parameter yang digunakan untuk mengontrol *trade-off* antara margin dan kesalahan klasifikasi adalah 1.
  - epsilon : Lebar toleransi yang digunakan untuk mengatur margin dalam *SVR* adalah 0.2.

  c. Kelebihan:

  - Efektif digunakan dalam regresi data dengan dimensi tinggi.
  - Mudah mengatasi *overfitting* dengan mengatur nilai C.

  d. Kekurangan:

  - Jika data mempunyai korelasi yang cenderung rendah, maka akan berpengaruh terhadap hasil.
  - Sensitif terhadap *imbalance data*.
</div>

## Evaluation
<div style="text-align: justify;">

Pada bagian ini, metrik evaluasi yang digunakan adalah *Root Mean Squared Error (RMSE)*.

*RMSE* bekerja dengan cara menghitung rata - rata kuadrat dari selisih hasil prediksi dan hasil sebenarnya, kemudian menghitung akar kuadrat dari rata - rata tersebut. Rumus *RMSE* adalah sebagai berikut :

$$
RMSE = \sqrt{\frac{1}{n} \sum_{i=1}^{n}(y_{\text{true}} - y_{\text{pred}})^2}
$$

Keterangan:

- $RMSE$ : Akar mean kuadrat galat (*Root Mean Squared Error*)
- $n$ : Jumlah data
- $y_{\text{true}}$ : Nilai sebenarnya
- $y_{\text{pred}}$ : Nilai prediksi

Sifat dari metrik ini adalah semakin kecil nilai *RMSE* yang dihasilkan maka mengindikasikan bahwa model tersebut semakin baik dalam memprediksi data.

Pelatihan data yang dilakukan menghasilkan data error sebagai berikut :
<center>

|     Model     |  *Train RMSE*  |  *Test RMSE*   |
|--------------|--------------|--------------|
| *RandomForest* | 13991.82     | 15934.24     |
| *KNN*          | 15508.45     | 20765.25     |
| *AdaBoost*     | 18440.61     | 19296.22     |
| *GradientBoost*| 26912.87     | 26140.51     |
| *SVM*          | 28879.35     | 27755.15     |

</center>

dari sini dapat ditarik kesimpulan bahwa **Random Forest** menghasilkan model terbaik, hal ini ditunjukkan dengan hasil *RMSE* train dan *RMSE* test paling kecil yaitu 13991.82 dan 15934.24.

Hasil dari setiap model yang sudah dilatih dilakukan percobaan terhadap 1 data test, kemudian memprediksi harga menggunakan masing - masing model sehingga hasilnya sebagai berikut :

<center>

| Model           | Prediksi Harga |
|-----------------|----------------|
| *y true*          | 129,990.00      |
| *KNN*             | 111,558.00     |
| *RandomForest*   | 133,498.56     |
| *AdaBoost*        | 127,207.64     |
| *GradientBoost*   | 75,569.51      |
| *SVM*             | 65,001.65      |

</center>
</div>

## Kesimpulan
<div style="text-align: justify;">

**Random Forest** menghasilkan model terbaik dan istem sudah dapat memprediksi harga dengan baik, meskipun masih terdapat *error*. Improvisasi yang dapat dilakukan adalah dengan melakukan *tuning hyperparameter*, melakuan evaluasi fitur - fitur yang digunakan, meningkatkan jumlah data, dan menggunakan model *machine learning* lain.
</div>

## Daftar Pustaka

[1] Surjuse V, et al. "Laptop Price Prediction using Machine Learning". https://ijcsmc.com/docs/papers/January2022/V11I1202229.pdf [accessed June 28 2023].

[2] Burke R. "How to Implement Random Forest, SVM, and Gradient Boosted Models for Time-to-Event Analyses". https://towardsdatascience.com/how-to-implement-random-forest-svm-and-gradient-boosted-models-for-time-to-event-analyses-5d79d8153bcd [accessed June 28 2023].

[3] Arslan E. "House Price Prediction". https://www.kaggle.com/code/emrearslan123/house-price-prediction [accessed June 28 2023].