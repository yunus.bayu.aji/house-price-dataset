# Laporan Proyek Machine Learning - Yunus Bayu Aji
<div style="text-align: justify;">

## Project Overview

Dalam era digital ini, memesan makanan tidak harus dilakukan dengan mendatangi restoran, tetapi memesan makanan dapat dilakukan melalui internet sesuai dengan kebutuhan dan preferensi konsumen. Dengan banyaknya menu yang ada, konsumen seringkali merasakan kebingungan dan bahkan tidak mungkin untuk memilah satu per satu makanan yang ada karena akan menghabiskan banyak sekali waktu. Oleh karenanya diperlukan pengembangan sebuah sistem untuk merekomendasikan makanan yang relevan terhadap konsumen.

Sistem rekomendasi sangat penting untuk di implementasikan oleh pelaku industri makanan karena mempunyai banyak manfaat baik dari pelaku industri maupun konsumen. Sistem rekomendasi yang baik akan meningkatkan pengalaman baik konsumen dalam memilih makanan. Selain itu, sistem rekomendasi makanan akan memberikan nilai tambah bagi industri makanan dengan meningkatkan penjualan dan promosi sehingga sistem ini akan memberikan dampak posistif dalam pemenuhan dan preferensi konsumen dan meningkatkan kinerja pelaku industri makanan *online*.

## Business Understanding
### Problem Statements

- Bagaimana cara mengembangkan sistem rekomendasi makanan yang sesuai dengan preferensi konsumen?

### Goals

- Mengembangkan sistem rekomendasi makanan yang relevan dan sesuai dengan preferensi pengguna.

### Solution statements
Solusi yang digunakan untuk mencapai goals yaitu menggunakan teknik *Content-Based Filtering* dan *Collaborative Filtering*.

- *Content-Based Filtering* menggunakan profil preferensi pengguna berdasarkan *item* yang disukai sebelumnya. Rekomendasi didapatkan berdasarkan kemiripan fitur antara *item* yang ada.

- *Collaborative Filtering* menggunakan kesamaan pola perilaku dan preferensi antar pengguna yang serupa dengan pengguna lain.

## Data Understanding
Data yang digunakan adalah data makanan yang memuat *ID* makanan, jenis makanan, vegetarian atau tidak, deskripsi, dan *rating*.

Dataset yang digunakan adalah [*Food Dataset*](https://gitlab.com/yunus.bayu.aji/house-price-dataset/-/raw/main/system%20recommendation/food.csv) dan [*Rating Dataset*](https://gitlab.com/yunus.bayu.aji/house-price-dataset/-/raw/main/system%20recommendation/ratings.csv).

### Variabel-variabel pada food dan rating dataset adalah sebagai berikut:

- Food_ID : *ID* makanan
- Name : Nama makanan
- C_Type : Jenis makanan
- Veg_Non : Apakah makanan vegetarian atau tidak
- Describe : Penjelasan mengenai makanan
- User_ID : *ID* pengguna atau konsumen
- Rating : Nilai yang diberikan konsumen (1-10)

Untuk memahami data lebih lanjut maka digunakan teknik *Exploratory Data Analysis(EDA)*. *EDA* dilakukan dengan 2 operasi utama yaitu Deskripsi Variabel dan Menangani *Missing Value*.

- Deskripsi Variabel : Pada tahap ini data akan divisualisasikan dalam bentuk tabel, kemudian dilakukan analisis secara langsung terhadap beberapa aspek data yaitu info data dan deskripsi data.

- Menangani *Missing Value* : Tahap ini dilakukan dengan pengecekan apakah dalam data terdapat data *null* dan *NaN*.

## Data Preparation
Teknik *data preparation* yang dilakukan adalah dengan menghilangkan sebagian teks yang tidak digunakan seperti tanda baca, kemudian melakukan penggabungan antara data Rating dan Makanan berdasarkan *ID* makanan.

Tahap pertama preparasi data adalah menghilangkan tanda baca. Pada dasarnya tanda baca tidak mempunyai makna tertentu dalam suatu kalimat, dalam hal ini tidak mempunyai kecenderungan apakah negatif atau positif, sehingga menghilangkan tanda baca tidak akan mempengaruhi proses pelatihan data atau operasi data.

Tahap kedua adalah menggabungkan data *rating* dan makanan. Kedua data ini terpisah tetapi mempunyai satu variabel data yang sama yaitu Food_ID, ini digunakan untuk melakukan pencocokan data, sehingga penggabungan dilakukan berdasarkan *ID* dari makanan.

Tahapan preparasi data sangat penting untuk melakukan persiapan terhadap data sehingga relevan untuk dilakukan analisis dan pemodelan. Menghasilkan data yang bersih dan terstruktur akan memberikan representasi model dengan lebih baik.

## Modeling

*Modelling* merupakan tahapan untuk melakukan pendefinisian algoritma yang digunakan, dalam hal ini digunakan 2 algoritma utama yaitu *Content-Based Filtering* dan *Collaborative Filtering*, penjelasan *modelling* kedua algoritma ini adalah sebagai berikut :

- *Content-Based Filtering* :
    
    Algoritma ini bekerja berdasarkan informasi dan anlisis karakteristik dari pengguna untuk kemudia melakukan rekomendasi item yang mempunyai tingkat kesamaan dengan item yang pernah dimasukkan sebelumnya.

    Algoritma ini bersifat mandiri dikarenakan tidak menggunakan data dari pengguna lain, dengan kata lain tidak diperlukan data yang banyak dan cenderung menggunakan informasi pengguna secara individual.
    
    Kelemahan dari algoritma ini adalah dikarenakan rekomendasi yang cenderung terbatas pada historis data pengguna sebelumnya, sehingga hasil rekomendasi yang diberikan cenderung terbatas. Sistem dapat memberikan rekomendasi dengan lingkup yang sangat spesifik, sehingga variasi data yang ditampilkan menjadi berkurang.

    *Modelling* pada tahap ini adalah membuat sebuah vektor matrix dari data Deskripsi Makanan, data matrix ini kemudian dihitung nilai *cosine similarity*-nya. Nilai ini yang kemudian digunakan sebagai referensi untuk mendapatkan data rekomendasi, dalam hal ini dibatasi sebanyak 7 buah rekomendasi.

    Top 7 hasil rekomendasi adalah sebagai berikut :

    <center>

    |index|foodName|foodDesc|
    |---|---|---|
    |0|methi chicken masala|Indian non-veg boneless chicken lemon juice red chilli powder salt ginger garlic paste curd red chilli powder salt coriander powder oil onion oil bay leaf green cardamom black cardamom cinnamon ginger garlic paste tomato kasoori methi green chilli water|
    |1|restaurant style fried chicken|Chinese non-veg egg salt white pepper maida salt red chilli powder garlic powder ginger powder onion powder oregano chillil flakes white pepper basil chicken drumsticks salt white pepper bread crumbs as required|
    |2|cheese chicken kebabs|Indian non-veg Chicken Thais Garlic Paste Garlic Paste Yellow Chilli Powder Cheese Curd Gram Flour Green Cardamom Powder Yellow Chilli Powder Mace Powder Nutmeg Powder Rock Salt Green Coriander Oil|
    |3|fish andlouse|French non-veg white wine and water mix to cover onion salt bay leaf black pepper corns olive oil onion garlic tomatoes peeled and seeded basil leaves spring fresh thyme  optional 1 bay leaf salt and pepper olive oil wine vinegar prepared mustard salt and pepper assorted garden herbs  parsley basil etc|
    |4|lamb korma|Indian non-veg onions almond paste ghee cinnamon sticks green cardamom cloves mace bay leaves garlic paste ginger paste lamb salt rose water infused with 45 strands of saffron yellow chilli powder yogurt yellow chilli powder black pepper powder coriander seed powder cumin powder red chilli powder turmeric powder clove powder green cardamom powder rose petal powder nutmeg powder black cardamom powder fennel powder cinnamon powder mace powder onions creammalai|
    |5|amritsari chicken masala|Indian non-veg chicken ginger garlic paste curd lemon juice vinegar coriander powder cumin powder red chilli powder salt onion butter red chilli powder coriander powder cumin powder ginger water salt green chilli tomatoes sugar butter  cream|
    |6|bihari fish curry|Indian non-veg rohu fish salt turmeric powder chilli powder oil garlic  green chillies mustard seeds black peppercorns cumin seeds whole red chillies fenugreek seeds tomatoes mustard oil bay leaves water garam masala coriander leaves|

    </center>

- *Collaborative Filtering*

    *Collaborative Filtering* bekerja dengan memanfaatkan data dari pengguna lain dalan suatu komunitas yang serupa. Algoritma ini melakukan analisis preferensi dari pengguna untuk kemudian dilakuan perbandingan kesamaan dengan pengguna lain yang mempunyai preferensi yang serupa.

    Kelebihan dari algoritma ini adalah variasi data yang direkomendasikan dapat lebih beragam, hal ini dikarenakan tidak terikat pada atribut pada item tertentu.

    Dikarenakan algoritma ini menggunakan data dari pengguna lain, sehingga preferensi antara pengguna yang tidak mempunyai profil yang mirip sulit untuk dilakukan, akibatnya kualitas rekomendasi dari pengguna lain kurang kredibel.

    *Modelling* pada tahap ini adalah membuat sebuah jaringan neural berbasis *embedding layer* dengan *regularizer* sebesar 1e-6, beberapa parameter yang digunakan adalah sebagai berikut :

    - embedding size = 42
    - loss = *Binary Crossentropy*
    - optimizer = *Adam*
    - learning_rate = 3e-3
    - metric = *Root Mean Squared Error*

    Top 10 hasil rekomendasi adalah sebagai berikut :

    <center>

    Rekomendasi Makanan Dengan *Rating* Tinggi Dari *User*

    |index|foodName|foodDesc|
    |---|---|---|
    |0|vegetable som tam salad|Healthy Food veg raw papaya carrot french bean diamond cherry tomato garlic crush mix chilli somtam dressing peanuts crushed peanuts|
    |1|summer squash salad|Dessert veg apples basmati rice nuscovado sugar you can also use normal sugar cashew nuts and almonds cassia bark or cinnamon stick red grapes|
    |2|egg in a blanket|Dessert veg Sugar egg yolks egg butter yeast milk|
    |3|spicy creamy kadai chicken|French veg for the cous cous plain couscous extra virgin olive oil vegetable stock herbs basil parsley thyme cilantro work best for the ratatouille olive oil regular not extra virgin red onions aubergines cut in to 3 cm cubes zucchini cut in to 3 cm cubes garlic cloves ground cumin sweet paprika tomato paste salt|
    |4|apple kheer|Healthy Food veg white balsamic vinegar lemon juice lemon rind red chillies garlic cloves crushed olive oil summer squash zucchini sea salt black pepper basil leaves|
    |5|chocolate doughnut|Indian non-veg chicken gingergarlic paste pepper powder lime juice oil salt tomatoes green chillies gingergarlic paste chilli powder black cardamoms cloves water onion ginger green chillies chilli powder turmeric powder garam masala kasturi methi cream|
    |6|couscous with ratatouille - tangy tomato sauce|Healthy Food non-veg olive oil chicken mince garlic minced onion salt black pepper carrot cabbage green onions sweet chilli sauce peanut butter ginger soy sauce fresh cilantro red pepper flakes crushed tarts|
    |7|bhurji- egg|Indian non-veg made using indian spices onion tomatoes green chilli and had with bread or parathas|
    |8|chicken minced salad|Mexican non-veg chicken oil salt and pepper paprika powder chilli flakes garlic paste onions bell peppers rice vegetable stock saffron peas olives parsley white wine|
    |9|chicken paella|French non-veg eggs brown bread slices butter chilli flakes oregano salt|

    Top 10 Rekomendasi Mananan

    |index|foodName|foodDesc|
    |---|---|---|
    |0|active charcoal modak|Beverage veg chamomile tea bags orange berries blueberries ginger mint leaves boiling water|
    |1|chocolate kaju katli|Japanese non-veg sole fillet you can also do 23 types of fish large shrimps optional potatoes peeled onions cut into slices garlic cloves red capsicum can use green also parsley or coriander choose the herb that most suits your taste buds few dashes of hot sauce paprika olive oil white wine rock salt|
    |2|sweet potato pie|Dessert veg Butter Castor Sugar Christmas Mix Spicy Cinnamon Powder Honey Glucose Cream Poultry Flour Baking Soda Bread Flour|
    |3|filter coffee|Dessert veg popped amaranth seeds jaggery almonds slivered unpeeled|
    |4|green cucumber shots|Mexican veg spinach onion cream garlic nutmeg salt lime juice artichoke hearts cubed polenta refined flour salt water butter flour salt pepper freshly ground|
    |5|green asparagus risotto|French non-veg lamb mince garlic  salt paprika powder pomodaro tomatoes olive oil celery shallots carrot black pepper bay leaf yellow chilli powder cilantro chicken thigh salt white pepper powder garlic paste white wine vinegar olive oil yellow chilli powder refined oil fenugreek seeds white pepper powder garlic fenugreek leaves onion broken cashew salt fresh cream kasoori methi|
    |6|bengali lamb curry|Italian veg carnaroli rice vegetable broth butter extra virgin olive oil parmigiano cheese onion minced white wine salt  pepper|
    |7|khichdi|Snack non-veg boneless skinless chicken thigh trimmed salt and pepper yogurt chilli powder ginger garlic paste coriander leaves oil to fry peri peri sauce potato fries|
    |8|strawberry quinoa pancakes|Healthy Food veg english cucumbers garlic cloves smashed romaine lettuce basil parsley cilantro big lemon sea salt olive oil|
    |9|peri peri chicken satay|Dessert veg cashew nuts sugar water milk chocolate or dark chocolate|
    </center>

## Evaluation
Evaluasi data dilakuakan pada masing - masing sistem yang telah dibuat. *Content-Based Filtering* menggunakan metrik *Precission* kemudian dihitung rata - rata untuk mengukur performa dari rekomendasi yang dihasilkan, sedangkan untuk *Colaborative Filtering* menggunakan *Root Mean Squared Error (RMSE)*.

### *Precission*

Pengukuran menggunakan metrik *Precision* dilakukan dengan melakukan perbandingan antara deskripsi makanan yang diinput oleh *user* dan deskripsi makanan hasil rekomendasi. Perbandingan dilakukan dengan *Cosine Similarity*, fungsi ini didefinisikan sebagai berikut :


$$
\text{{cosine\_similarity}}(\mathbf{A}, \mathbf{B}) = \frac{{\mathbf{A} \cdot \mathbf{B}}}{{\|\mathbf{A}\| \cdot \|\mathbf{B}\|}}
$$

Keterangan:

- $\mathbf{A}$ dan $\mathbf{B}$: Vektor yang mewakili dua dokumen atau teks yang ingin dibandingkan.
- $\cdot$: Operasi dot product antara vektor $\mathbf{A}$ dan $\mathbf{B}$.
- $\|\mathbf{A}\|$ dan $\|\mathbf{B}\|$: Norma Euclidean dari vektor $\mathbf{A}$ dan $\mathbf{B}$ masing-masing.

Nilai yang dihasilkan dari setiap data kemudian dilakukan perbandigan untuk menentukan apakah hasil rekomendasi sudah sesuai, jika sesuai makan nilai akan dianggap sebagai 1 dan jika tidak sesuai makan akan dianggap 0, sesuai dalam hal ini berarti nilai similarity lebih dari threshold, dimana treshold ditentukan sebesar 0.25. Algoritma lengkapnya adalah sebagai berikut :

$$
\begin{align*}

& \text{Untuk setiap elemen } x \text{ dalam daftar Makanan:} \\
& food_{\text{desc}} = \text{get\_food\_description}(x) \\
& food\_recommended = \text{get\_recommendation}(x) \\
& \text{Untuk setiap elemen } y \text{ dalam daftar } \text{food\_recommended:} \\
& \quad \quad food\_recommended_{\text{desc}} = \text{get\_recommendation}(y)\\
& \quad \quad similarity\_score = cosine\_similarity(food_{\text{desc}},  food\_recommended_{\text{desc}})\\
& \quad \quad \text{{all\_score}} =
\begin{cases}
1, & \text{{jika }} \text{{similarity\_score}} > threshold \\
0, & \text{{jika }} \text{{similarity\_score}} \leq threshold
\end{cases}
\end{align*}
$$

- $x$: Elemen dalam daftar Makanan.
- $food_{\text{desc}}$: Deskripsi makanan yang diperoleh dari fungsi $\text{get\_food\_description}$.
- $\text{food\_recommended}$: Daftar makanan yang direkomendasikan untuk makanan $x$.
- $y$: Elemen dalam daftar $\text{food\_recommended}$.
- $food\_recommended_{\text{desc}}$: Deskripsi makanan yang diperoleh dari fungsi $\text{get\_recommendation}$ untuk makanan $y$.
- $\text{similarity\_score}$: Skor kesamaan (cosine similarity) antara $food_{\text{desc}}$ dan $food\_recommended_{\text{desc}}$.
- $\text{threshold}$: Ambang batas untuk menentukan apakah $\text{similarity\_score}$ lebih besar dari $\text{threshold}$ atau tidak.
- $\text{all\_score}$: Array yang berisi nilai-nilai 1 atau 0, tergantung pada apakah $\text{similarity\_score}$ lebih besar dari $\text{threshold}$ atau tidak.

Nilai dari keseluruhan data yang sudah didapatkan, kemudian dilakukan perhitungan rata - rata yang selanjutnya direpresentasikan sebagai nilai *precission*, rumus rata rata yang digunakan adalah sebagai berikut :

$$
\begin{align*}
Precission = \frac{{\sum_{i=1}^{|all\_score|} all\_score[i]}}{|all\_score|}
\end{align*}
$$

- $\text{Precission}$: Presisi, yang merupakan rata-rata dari semua nilai dalam array $\text{all\_score}$.
- $\sum_{i=1}^{|all_score|} \text{all\_score}[i]$: Penjumlahan dari semua nilai dalam array $\text{all\_score}$.
- $|all_score|$: Jumlah elemen dalam array $\text{all\_score}$, yaitu panjang array $\text{all\_score}$.

Dalam hal ini, performa yang didapatkan adalah **0.568**

### *Root Mean Squared Error(RMSE)*

*RMSE* merupakan alat ukur untuk melakukan evaluasi pada kasus pemodel menggunakan prinsip regresi. Metrik ini mengukur performa dari model dengan menghitung akar dari rata - rata kuadrat selisih antara nilai prediksi dan nilai sebenarnya. 

Fungsi *RMSE* didefinisikan sebagai berikut :

$$
RMSE = \sqrt{\frac{1}{n} \sum_{i=1}^{n} (y_{\text{true}} - y_{\text{pred}})^2}
$$

Keterangan:

- $RMSE$: Akar mean kuadrat galat (*Root Mean Squared Error*).
- $n$: Jumlah data.
- $y_{\text{true}}$: Nilai sebenarnya.
- $y_{\text{pred}}$: Nilai prediksi.

hasil dari training data ditunjukkan oleh grafik berikut :

<center>

![Nama Gambar](https://gitlab.com/yunus.bayu.aji/house-price-dataset/-/raw/main/system%20recommendation/graph.png)

</center>

dari grafik di atas menghasilkan nilai *RMSE* untuk data train sebesar **0.1477** dan *RMSE* untuk data validation sebesar **0.3522**.

## Kesimpulan

- Sistem rekomendasi telah dapat dibuat dengan menggunakan 2 metode pendekatan yaitu Content-Based Filtering dengan nilai performa **0.568** dan Colaborative Filtering dengan nilai performa *RMSE* *train* dan *validation* sebesar **0.1477** dan **0.3522**.
- Sistem rekomendasi dapat menghasilkan *top* N rekomendasi dengan cukup baik menggunakan 2 metode yang digunakan.
- Overfitting dapat diatasi dengan *tuning hyperparameter*, menambah fitur data yang digunakan dan menambah jumlah data.

## Daftar Pustaka

[1] Chouinard J.C. "How To Build A Recommender System With TF-IDF And NMF (Python)". https://www.searchenginejournal.com/topic-clusters-recommender-system/436123/ [Diakses pada 1 Juli 2023]

[2] Chhipa S, et al. "Recipe Recommendation System Using TF-IDF". https://www.itm-conferences.org/articles/itmconf/pdf/2022/04/itmconf_icacc2022_02006.pdf[Diakses pada 1 Juli 2023]

[3] Dicoding. "Kelas Machine Learning Terapan". https://www.dicoding.com/academies/319 [Diakses pada 1 Juli 2023]

[4] Schemersays. "Food Recommendation System". https://www.kaggle.com/datasets/schemersays/food-recommendation-system?select=1662574418893344.csv [Diakses pada 1 Juli 2023]
</div>