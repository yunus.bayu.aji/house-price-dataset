# -*- coding: utf-8 -*-
"""Proyek Pertama : Predictive Analytics.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/19x5Wb01rPNNRgOnqpA2NFBPZqd6NEO9d

# Mendefinisikan Library

```
Meng-install library art untuk memberikan bentuk print out khusus
```
"""

!pip install art

"""

```
Dependencies:

*   numpy : Library untuk melakukan operasi matematikan dan manipulasi array
*   matplotlib : Visualisasi data
*   pandas : Data manipulasi dan analisis
*   seaborn : Visualisasi data
*   art : Mencetak ASCII art
*   IPython.display : Menampilkan data grafik dan tabel di IPython Environments
*   sklearn : Melakukan operasi dan algoritma untuk Predictive Analytics
*   warnings : Modul untuk mengatasi warning


```

"""

# Commented out IPython magic to ensure Python compatibility.
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
# %matplotlib inline
import seaborn as sns

from art import *
from IPython.display import display

from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split, cross_val_score, KFold
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor, GradientBoostingRegressor
from sklearn.svm import SVR

import warnings
warnings.filterwarnings('ignore')

from google.colab import data_table
data_table.enable_dataframe_formatter()

"""

```
Class Config merupakan Class yang berisikan data variabel untuk mengatur parameter yang digunakan pada serangkaian proses Predictive Analytics
```

"""

class Config:

  """
  Class yang digunakan untuk mengatur konfigurasi dan parameter

  ...

  Attributes
  ----------
  url : str
      Path atau URL dataset
  models : dict
      Dictionary dari inisialisasi model - model untuk mempelajari data
  """

  url = "https://gitlab.com/yunus.bayu.aji/house-price-dataset/-/raw/main/laptopPrice.csv"

  models = {
      "KNN": KNeighborsRegressor(n_neighbors=15),
      "RandomForest": RandomForestRegressor(n_estimators=100, max_depth=7, random_state=1605, n_jobs=-1),
      "AdaBoost": AdaBoostRegressor(n_estimators=100, random_state=7, learning_rate=1e-3),
      "GradienBoost": GradientBoostingRegressor(n_estimators=100, learning_rate = 1e-3, random_state = 7),
      "SVM": SVR(C=1.0, epsilon=0.2)

  }

"""

```
Class yang berisikan fungsi - fungsi untuk menjalankan Predictive Analytics

Attributes
----------
url : str
    Path atau URL dataset

Methods
-------
get_data(self, data, index_state = False)
    Menampilkan keseluruhan data

get_data_shape(self, data)
    Menampilkan ukuran data

get_data_info(self, data)
    Menampilkan informasi data

get_data_describe(self, data)
    Menampilkan deskripsi data

get_data_head(self, data)
    Menampilkan 5 baris pertama data

get_nan_value(self, data)
    Menampilkan jumlah NaN data

get_num_duplicate(self, data)
    Menampilkan jumlah data terduplikasi

variable_description(self)
    Menampilkan keseluruhan deskprisi variabel (ukuran data, informasi data, deskripsi data, head data, NaN data, dan Duplikasi data)

missing_value_handling(self)
    Melakukan operasi untuk menghilangkan duplikasi data dan mengatasi Outlier

univariate_analysis(self)
    Menampilkan grafik dan persentase data dari Data Numerik dan Data Kategorik sehingga dapat dilakukan analisis

missing_value_handling(self)
    Menampilkan grafik dari beberapa data yang saling berkorelasi untuk dilakukan analisis seperti Barplot, Pairplot, Heatmap, dan melakukan Eliminasi data

data_preparation(self)
    Melakukan encoding data kategorikal dan membagi data menjadi data train dan test

train(self, models: dict)
    Melakukan Training dan Testing data menggunakan beberapa algoritma untuk mempelajari pola data

train(self, models: dict)
    Melakukan evaluasi data
```

"""

class Predictive_Analytics:

  def __init__(self, url):
    tprint("1. Data Loading")
    print("Read Data From ", url)

    self.data = pd.read_csv(url)

    print()

  def get_data(self, data, index_state = False):
    print("House Price Data")
    display(data_table.DataTable(data, include_index = index_state, num_rows_per_page=10))
    print()
    return data

  def get_data_shape(self, data):
    print("Data Shape")
    print(data.shape)
    print()
    return data.shape

  def get_data_info(self, data):
    print("Data Info")
    # display(data_table.DataTable(data.info(), include_index=False, num_rows_per_page=10))
    print()
    return data.info()

  def get_data_describe(self, data):
    print("Data Describe")
    display(data_table.DataTable(data.describe(), include_index=True, num_rows_per_page=10))
    print()
    return data.describe()

  def get_data_head(self, data):
    print("Data Head")
    display(data_table.DataTable(data.head(), include_index=False, num_rows_per_page=10))
    print()
    return data.head()

  def get_nan_value(self, data):
    print("Check NaN Value")
    display(self.data.isna().sum())
    print()

  def get_num_duplicate(self, data):
    print("Number of Duplicated Data")
    print(self.data.duplicated().sum())
    print()

  def variable_description(self):
    tprint("2. EDA - Deskripsi Variabel")
    self.get_data(self.data)
    self.get_data_head(self.data)
    self.get_data_info(self.data)
    self.get_data_describe(self.data)
    self.get_data_shape(self.data)
    self.get_nan_value(self.data)
    self.get_num_duplicate(self.data)
    print()

  def missing_value_handling(self):
    tprint("3. EDA - Menangani Missing Value")

    print("Drop Duplicate Data")
    print()
    self.data.drop_duplicates()

    print("Handling Outlier")

    self.Q1 = self.data.quantile(0.25)
    self.Q3 = self.data.quantile(0.75)
    self.IQR = self.Q3-self.Q1
    self.data = self.data[~((self.data<(self.Q1-1.5*self.IQR))|(self.data>(self.Q3+1.5*self.IQR))).any(axis=1)]

    self.get_data_shape(self.data)
    self.get_data_describe(self.data)

  def univariate_analysis(self):
    tprint("4. EDA - Univariate Analysis")
    self.numeric_features = self.data.select_dtypes(exclude='object').columns.to_list()
    self.categoric_features = self.data.select_dtypes(include='object').columns.to_list()
    print("Numerical Features : ", self.numeric_features)
    print("Categorical Features : ", self.categoric_features)
    print()

    print("Categorical Feature Graph")
    print()
    for i,x in enumerate(self.categoric_features):
      self.feature = self.categoric_features[i]
      self.count = self.data[self.feature].value_counts()
      self.percent = 100*self.data[self.feature].value_counts(normalize=True)
      self.df = pd.DataFrame({'Number of Sample':self.count, 'Percentage':self.percent.round(1)})
      print(self.df)
      display(self.count.plot(kind='bar', title=self.feature))
      plt.show()

    print("Numerical Feature Graph")
    print()
    display(self.data.hist(bins=100, figsize=(20,5)))
    plt.show()

  def multivariate_analysis(self):
    tprint("5. EDA - Multivariate Analysis")
    print()

    print("Barplot Analysis")

    self.categoric_features = self.data.select_dtypes(include='object').columns.to_list()
    for i,x in enumerate(self.categoric_features):
      self.group_data = self.data.groupby(x)
      self.ax = sns.barplot(x = self.group_data["Price"].mean().index,
                  y = self.group_data["Price"].mean().values,
                  palette = "summer")
      for wo in self.ax.containers:
        self.ax.bar_label(wo, color='green', size=10)
      plt.title("Average 'Price' Relative to {}".format(x))
      plt.xlabel(x)
      plt.ylabel("Price")
      plt.show()

    print("Pairplot Analysis")

    sns.pairplot(self.data, diag_kind = 'kde')

    print("Heatmap Correlation")
    plt.figure(figsize=(10, 8))
    self.correlation_matrix = self.data.corr().round(2)

    self.cmap = sns.color_palette("rocket_r", as_cmap=True)
    sns.heatmap(data=self.correlation_matrix, annot=True, cmap=self.cmap)
    plt.title("Matrix Correlation of Numeric Features", size=20)

    print("Eliminating Small Correlation Data")

    self.eliminate_data = self.correlation_matrix["Price"][~(self.data.corr()["Price"]>0.50) | (self.data.corr()["Price"]<-0.50)].index.to_list()
    for x in self.eliminate_data:
      self.data.drop([x], inplace=True, axis=1)
    self.get_data_info(self.data)

  def data_preparation(self):
    tprint("6. Data Preparation")

    print("Encoding Categorical Data : ")
    self.categoric_features = self.data.select_dtypes(include='object').columns.to_list()
    print(self.categoric_features)
    print()
    for x in self.categoric_features:
      self.data = pd.concat([self.data, pd.get_dummies(self.data[x], prefix=x)],axis=1)
    self.data.drop(self.categoric_features, axis=1, inplace=True)
    self.get_data_info(self.data)
    self.get_data_head(self.data)

    self.X = self.data.drop(["Price"],axis =1)
    self.Y = self.data["Price"]
    self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(self.X,
                                                                            self.Y,
                                                                            test_size = 0.2,
                                                                            random_state = 1609)

    print("Total Dataset Input  : ", len(self.X))
    print("Total Dataset Output : ", len(self.Y))
    print("Total Train Data     : ", self.x_train.shape)
    print("Total Test Data      : ", self.x_test.shape)

  def train(self, models: dict):
    tprint("7. Training Data")

    k_folds = KFold(n_splits= 7)
    self.results = {f"{n}":None for n,m in models.items()}
    self.results2 = self.results.copy()
    for name, model in models.items():
      print("Train model using ", name)
      model.fit(self.x_train, self.y_train)
      self.results[name] = np.mean(np.sqrt(-cross_val_score(model,
                                                       self.x_train,
                                                       self.y_train,
                                                       scoring = 'neg_mean_squared_error',
                                                       cv= k_folds)))
      self.results2[name] = np.mean(np.sqrt(-cross_val_score(model,
                                                       self.x_test,
                                                       self.y_test,
                                                       scoring = 'neg_mean_squared_error',
                                                       cv= k_folds)))
    self.results = pd.DataFrame(self.results, index=["RMSE Train"]).T
    self.results = pd.concat([self.results, pd.DataFrame(self.results2, index=["RMSE Test"]).T], axis=1)
    self.results = self.results.sort_values(by="RMSE Train", ascending=True)
    self.get_data(self.results, index_state = True)

  def evaluate(self, models: dict):
    tprint("8. Evaluation")

    self.index = 1

    self.test_input = self.x_test.iloc[self.index].values.reshape(1, -1)
    print("y true : ", self.y_test.iloc[self.index])
    for name, model in models.items():
        print(f"Prediksi {name} : ", model.predict(self.test_input).round(2)[0])

conf = Config()

# data = pd.read_csv(conf.url)
# data.info()
# data_table.DataTable(data, include_index=False, num_rows_per_page=10)
pa = Predictive_Analytics(url = conf.url)

"""

```
Dari Deskripsi Variabel, dapat ditarik beberapa kesimpulan
  a. Data terdiri dari 18 fitur data dan 1 output yaitu "Price"
  b. Data terdiri dari 2 tipe data utama yaitu object (kategorikal) dan int64 (numerikal)
  c. Tidak terdapat NaN value
  d. Terdapat 21 duplikasi data
  e. Terdapat Outlier  
```

"""

pa.variable_description()

"""

```
Program missing_value_handling menghilangkan duplikasi data dan outlier sehingga data menjadi berjumlah 634 data
```

"""

pa.missing_value_handling()

"""

```
Dari Univariate Analysis dapat ditarik kesimpulan bahwa "Number of Ratings"
dan "Numer of Reviews" tidak begitu mempengaruhi harga, hal ini dapat dilihat
dari perubahan grafik yang cenderung menurun tetapi "Price" cenderung fluktuatif
```

"""

pa.univariate_analysis()

"""

```
Dari matriks korelasi terlihat secara kuantitatif bahwa "Number of Ratings"
dan "Numer of Reviews" hanya sedikit berpengaruh dalam hal ini >-0.5
atau <0.5 dan cenderung mendekati nol (0), sehingga data numerik ini dihilangkan dari data fitur
```

"""

pa.multivariate_analysis()

"""

```
Data kategorikal dilakukan encoding dan data dibagi kedalam data train dan test
dengan perbandingan 4:1
```

"""

pa.data_preparation()

"""

```
Training data dilakukan dengan 5 model untuk mempelajari pola data yaitu KNN,
Random Forest, AdaBoost, GradienBoost, dan SVM. metriks yang digunakan adalah
RMSE dengan hasil akhir Random Forest mempunyai nilai error paling kecil
```

"""

pa.train(conf.models)

"""

```
Hasil dari evaluasi data setiap model yang telah di latih
```

"""

pa.evaluate(conf.models)